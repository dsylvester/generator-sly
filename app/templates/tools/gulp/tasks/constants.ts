export const RootDir  = "./";
export const SrcDir = "./src";
export const TsRoot = "./src";
export const DestDir = "./dist/";
export const BuildDir = "./build/";
export const BundleDir = "./bundle";
export const AngularComponentsHtmlFiles = `${TsRoot}/**/*.html`;
export const AngularComponentsLessFiles = `${TsRoot}/**/*.less`;

