let Generator = require('yeoman-generator');
let mkdir = require('mkdirp');
let _ = require("lodash");


module.exports = class extends Generator {


    contructor(args, opts) {


        this.argument('componentName', { required: true, type: String, description: "Enter your Component's Name" });       


    }

    initializing() {

        this.moduleRootDir = "";
        this.ComponentName = "";
        this.ComponentRootDir = "";
        this.HtmlRootDir = "";
        this.tmpl = this.sourceRoot();



    }

    prompting() {


    }

    configuring() {


    }

    default() {

        let tempComponentName = this.options.componentName;

        let componentName = _.snakeCase(tempComponentName);
        this.ComponentName = _.capitalize(componentName);
        this.ComponentNameLc =  _.chain(this.ComponentName).lowerCase();
        this.ComponentNameCamelSnakeCase =  this._checkForSpace(_.chain(this.ComponentName).lowerCase());
        this.ComponentNameNoSpaces = this._pascalCaseNoSpaces(_.chain(this.ComponentName).lowerCase());

        let componentRootDir = "";

        // console.log(`componentName: ${componentName}`);
        // console.log(`this.ComponentName: ${this.ComponentName}`);        
        // console.log(`this.ComponentNameCamelSnakeCase: ${this.ComponentNameCamelSnakeCase}`);
        // console.log(`this.ComponentNameNoSpaces: ${this.ComponentNameNoSpaces}`);

        // Make sure the User is in the Component Directory
        // or Change Directory to the Componnent directory

        // DestRoot = C:\temp\testing\test_123\src\assets\ts\Location_profile
        let destRoot = this.destinationRoot();
        // console.log(`destinationRoot: ${this.destinationRoot()}`);

        let needToChangeToComponentDirectory = false;


        if (!this._usingOrInComponentDirectory()) {
             
            this._tryChangeToComponentDirectory();
            componentRootDir = `${this.destinationRoot()}\\Components`;
            // console.log(`Current Path: ${this.destinationRoot()}`);
            // console.log(`Component Dircetory: ${componentRootDir}`);
            
            this.ComponentRootDir = componentRootDir;           
            this.HtmlRootDir = `${this.destinationRoot()}\\Html`;
            
        }
        
        



    }


    writing() {

        this._createNewComponent();
    }


    conflicts() { }

    install() { }

    end() { }

    _createNewComponent() {

        this._copyIndexTypescriptFile();
        this._copyComponentFile();
        this._copyComponentHtmlFile();
        this._copyComponentLessFile();
    }

    
    _usingOrInComponentDirectory() {

        // DestRoot = C:\temp\testing\test_123\src\assets\ts\Location_profile
        let destRoot = this.destinationRoot();

        let valid = true;

        if (destRoot.toLowerCase().indexOf("components") !== -1 &&
            this._lastElementIsEqual(destRoot.toLowerCase(), "components")) {
            return valid;
        }

        valid = false;

        return valid; 
    }

    _lastElementIsEqual(path, searchName) {
        
        let seperator = "";

        if (path.indexOf("/") !== -1) {
            seperator = "/";
        }
        else {
            seperator = "\\";
        }

        let temp = path.split(seperator);  

        return temp[temp.length - 1] === searchName;

    }

    _tryChangeToComponentDirectory() {

        try {
            process.chdir("components");
            console.log("I changed to Component Directory");
        }

        catch(e) {
            throw new Error("You must be in the Components directory to create a new Component");

        }
    }

    _indexTypescriptFileExists() {
       return this.fs.exists(`${this.ComponentRootDir}\\index.ts`);
    }

    _copyIndexTypescriptFile() {

        // console.log(`ComponentRootDir: ${this.ComponentRootDir}`);

        if (!this._indexTypescriptFileExists()) {
            this.fs.copyTpl(
                `${this.tmpl}\\component.index.ts`,
                `${this.ComponentRootDir}\\index.ts`,
                {
                    componentName: this.ComponentNameNoSpaces,
                    ComponentNameCamelSnakeCase: this.ComponentNameNoSpaces
                }
            );
        }
    }

    _copyComponentFile() {
        this.fs.copyTpl(
            `${this.tmpl}\\component.ts`,
            `${this.ComponentRootDir}\\${this.ComponentNameNoSpaces}.ts`,
            {
                componentName: this.ComponentNameNoSpaces,
                ComponentNameCamelSnakeCase: this.ComponentNameNoSpaces
            }
        );
    }

    _copyComponentHtmlFile() {
        this.fs.copyTpl(
            `${this.tmpl}\\component.html`,
            `${this.HtmlRootDir}\\${this.ComponentNameNoSpaces}.html`,
            {
                componentName: this.ComponentNameNoSpaces,
                ComponentNameCamelSnakeCase: this.ComponentNameNoSpaces
            }
        );
    }

    _copyComponentLessFile() {
        this.fs.copy(
            `${this.tmpl}\\component.less`,
            `${this.HtmlRootDir}\\${this.ComponentNameNoSpaces}.less`,
            {
                componentName: this.ComponentName,
                componentNameLc: this.ComponentNameLc
            }
        );
    }

    _checkForSpace(val) {
        // console.log(`Val: ${val}`);
        // console.log(`val.indexOf ${val.indexOf(" ")}`);

        if (val.indexOf(" ") !== -1) {
            let temp = String(val).split(" ");

            // console.log(temp);
            let result = "";

            var tempResult = temp.forEach((x) => {
                result = result + `${_.capitalize(x)}_`;
                // console.log(`Result: ${result}`);
            });

            return result.substring(0, result.length - 1);
        }
    }

    _pascalCaseNoSpaces(val) {
        // console.log(`Val: ${val}`);
        // console.log(`val.indexOf ${val.indexOf(" ")}`);

        if (val.indexOf(" ") !== -1) {
            let temp = String(val).split(" ");

            // console.log(temp);
            let result = "";

            var tempResult = temp.forEach((x) => {
                result = result + `${_.capitalize(x)}`;
                // console.log(`Result: ${result}`);
            });

            return result.substring(0, result.length);
        }
    }

};

