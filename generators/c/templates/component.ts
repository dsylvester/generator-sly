import { Component, OnInit } from "@angular/core";



@Component({
    selector: "",
    moduleId: __moduleName,
    templateUrl: "../Html/<%= ComponentNameCamelSnakeCase %>.html",
    styleUrls: [
        "../Html/<%= ComponentNameCamelSnakeCase %>.css"
    ],
    animations: [

    ]
})


export class <%= componentName %> implements OnInit  {

    // Properties and Fields

    
    constructor() {

        this.Init();

    }

    private Init(): void {

    }

    public ngOnInit(): void {

    }

    
}
