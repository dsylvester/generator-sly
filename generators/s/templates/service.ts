import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/Rx";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";



@Injectable()
export class <%= ServiceNameCamelSnakeCase %>Service {

    // Properties and Fields
    private headers: HttpHeaders;


    constructor(private http: HttpClient) {
        
        this.Init();
    }

    private Init() {

            this.createHeaders();

    }

    private createHeaders(): void {

        this.headers = new HttpHeaders({
            "content-type": "application/json",
            "Accept": "application/json",
        });
    }
    
    public Get(): Array<<%= ServiceNameCamelSnakeCase %>> {

    }

    public Get(id: number|string): <%= ServiceNameCamelSnakeCase %> {
        
            }

    public Post(data: <%= ServiceNameCamelSnakeCase %>): <%= ServiceNameCamelSnakeCase %> {
        
    }

    public Put(id: number|string): <%= ServiceNameCamelSnakeCase %> {
        
    }

    public Delete(id: number|string): void {
        
    }

    


}
